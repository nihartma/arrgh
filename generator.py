"""
"its like somebody took a transcript of a couple arguing at ikea and
made random edits until it compiled without errors"
"""
from __future__ import print_function

import ROOT
import math
import re
import itertools
import copy
import os

# --------------------------------------------------
# Classes
# --------------------------------------------------

class cutvar:
    def __init__(self, expr, type, cutvalues, **kwargs):
        self.comparator = kwargs.get("comparator",">")
        if "varname" in kwargs:
            self.varname = kwargs["varname"]
        else:
            self.varname = expr
        if type == "expr":
            self.vars = kwargs["vars"]
        if "vector<" in type:
            self.varname = expr.replace("[","_").replace("]","")
        self.expr = expr
        self.type = type
        self.cutvalues = sorted(cutvalues)
        self.ncuts = len(cutvalues)
        self.low = 1 if self.comparator == ">" else 0

class bin:
    def __init__(self, expr, vars=[], **kwargs):
        self.expr = expr
        self.vars = vars

class generator:
    def __init__(self, name, **kwargs):
        self.cuts = []
        self.bins = []
        self.weight = "1.0"
        self.vars = []
        self.presel = "1"
        self.trees = []
        self.treenames = []
        self.name = name
        self.txtfile = self.name+".txt"
        self.f = None # opened file
        self.workdir = kwargs.get("workdir", ".")
        self.doDebug = kwargs.get("doDebug", False)
        self.splitTrees = kwargs.get("splitTrees", False)
        self.statusInterval = kwargs.get("statusInterval", 10000)
        self.subGens = []
        self.binGens = []

    def setWorkdir(self, workdir):
        self.workdir = workdir

    def addTree(self, filename, treename, **kwargs):
        name = kwargs.get("name", None)
        self.trees.append((filename, treename))
        self.treenames.append(name)

    def addCuts(self, *args, **kwargs):
        self.cuts.append(cutvar(*args, **kwargs))

    def addBin(self, *args, **kwargs):
        self.bins.append(bin(*args, **kwargs))

    def getVars(self):
        """
        return all vars that need to be defined
        and their addresses be passed to TTree
        """
        vars = []
        for cut in self.cuts:
            if cut.type == "expr":
                for var,type in cut.vars:
                    if "vector<" in type:
                        var = var.split("[")[0]
                    if not (var, type) in vars:
                        vars.append((var, type))
            elif "vector<" in cut.type:
                vector_varname = cut.expr.split("[")[0]
                if not (vector_varname, cut.type) in vars:
                    vars.append((vector_varname, cut.type))
            else:
                if not (cut.varname, cut.type) in vars:
                    vars.append((cut.varname, cut.type))
        for var in self.vars:
            if not var in vars:
                vars.append(var)
        for bin in self.bins:
            for var in bin.vars:
                if not var in vars:
                    vars.append(var)
        return vars

    def setWeightExpression(self, expr, vars):
        self.weight = expr
        self.vars += vars

    def addVar(self, var, type):
        self.vars.append((var, type))

    def setPreselectionExpression(self, expr, vars):
        self.presel = expr
        self.vars += vars

    def make1dIndexExpression(self, indexnames, indexranges):
        """
        return c expression to calculate
        one index out of n indices
        """
        # special case for only 1 variable
        if len(indexnames) == 1:
            return "i{0}".format(indexnames[0])
        # else
        funexpr = "i{0}+({1}*(".format(indexnames[0], indexranges[0])
        for name, range in zip(indexnames[1:-1], indexranges[1:-1]):
            funexpr +="i{0}+({1}*(".format(name, range)
        funexpr += "i{0}".format(indexnames[-1])
        funexpr += "{}".format(")"*(len(indexnames)-1)*2)
        return funexpr

    def get1dIndex(self, l, ranges):
        """
        returns cut combination number

        Arguments:
        l (list): list of cuts
        ranges (list): list of number of cut values for each cut
        """
        i1d = l[0]
        factor = ranges[0]
        for i, max in zip(l[1:], ranges[1:]):
            i1d += i*factor
            factor *= max
        return i1d

    def getNdIndex(self, i, ranges):
        indices = []
        factor = 1
        mod = i
        for j in ranges:
            factor *= j
        for j in reversed(ranges):
            factor = factor/j
            div = int(mod)//int(factor)
            mod = int(mod)%int(factor)
            indices.append(div)
        return [i for i in reversed(indices)]

    def getCutExprFromIndex(self, i, **kwargs):
        removeCut = kwargs.get("removeCut","")
        removeCuts = kwargs.get("removeCuts",[])
        ranges = [c.ncuts for c in self.cuts]
        indices = self.getNdIndex(i, ranges)
        first = 1
        expr = ""
        for n, cut in zip(indices, self.cuts):
            if cut.varname == removeCut:
                continue
            if cut.varname in removeCuts:
                continue
            if not first:
                expr += "&&"
            else:
                first = 0
            expr += "{}{}{}".format(cut.expr,
                                    cut.comparator,
                                    cut.cutvalues[n])
        return expr

    def getnCutExprFromIndex(self, i, **kwargs):
        removeCut = kwargs.get("removeCut","")
        ranges = [c.ncuts for c in self.cuts]
        indices = self.getNdIndex(i,ranges)
        first =1
        expr = ""
        for n, cut in zip(indices,self.cuts):
            if cut.varname ==removeCut:
                continue
            if not first:
                expr += "&&"
            else:
                first =0
            expr += "{}{}{}".format(cut.comparator,
                cut.cutvalues[n])
        return expr.replace(" ","")

    def getYieldUsingProject(self, cutCombination, **kwargs):
        """
        cross check cut combination by fetching yield
        and error using TTree::Project
        """
        cutexpr = kwargs.get("cutexpr", "({})*({})".format(self.getCutExprFromIndex(cutCombination), self.presel))
        t = ROOT.TChain()
        for filename, treename in self.trees:
            t.AddFile(filename, -1, treename)
        helphist = ROOT.TH1F("h","",1,0,2)
        helphist.Sumw2()
        err = ROOT.Double()
        n = t.Project("h","1","({})*{}".format(cutexpr, self.weight))
        yie = helphist.IntegralAndError(0, helphist.GetNbinsX(), err)
        return (float(n), yie, err)

    def getYieldFromFile(self, cutn, txtfile):
        with open(txtfile) as f:
            for l in f:
                if l.startswith("{} ".format(cutn)):
                    return (float(l.strip().split()[1]),
                            float(l.strip().split()[2]),
                            float(l.strip().split()[3])) # raw, y, dy

    def getYieldFromFiles(self, cutn):
        r, y, dy2 = (0.0, 0.0, 0.0)
        if self.splitGen():
            for subGen in self.subGens:
                _r, _y, _dy = subGen.getYieldFromFiles(cutn)
                r, y, dy2 = (r+_r, y+_y, dy2+_dy*_dy)
        elif self.splitBins():
            for binGen in self.binGens:
                _r, _y, _dy = binGen.getYieldFromFiles(cutn)
                r, y, dy2 = (r+_r, y+_y, dy2+_dy*_dy)
        else:
            return self.getYieldFromFile(cutn, self.workdir+"/_txt/"+self.txtfile)
        return r, y, math.sqrt(dy2)

    def printYields(self, cutn):
        if self.splitGen():
            for subGen in self.subGens:
                subGen.printYields(cutn)
        if self.splitBins():
            for binGen in self.binGens:
                binGen.printYields(cutn)
        r, y, dy = self.getYieldFromFiles(cutn)
        print("{}: {:.2g}+/-{:.2g} ({:.0f})".format(self.name, y, dy, r))

    def printYieldsProject(self, cutn, **kwargs):
        if self.splitGen():
            for subGen in self.subGens:
                subGen.printYieldsProject(cutn, **kwargs)
        if self.splitBins():
            for binGen in self.binGens:
                binGen.printYieldsProject(cutn, **kwargs)
        r, y, dy = self.getYieldUsingProject(cutn, **kwargs)
        print("{}: {:.2g}+/-{:.2g} ({:.0f})".format(self.name, y, dy, r))

    def getBestSignificance(self, sigGen, bkgGen, **kwargs):
        bkgsys = kwargs.get("bkgsys", 0.25)
        require2bg = kwargs.get("require2bg", False)
        max = 0
        maxi = 0
        ncombinations = sigGen.getNCombinations()
        for (__, _is, rs, s, ds), (info, i, rb, b, db) in itertools.izip(getYields(sigGen), getYields(bkgGen)):
            if not i%100000:
                print("Calculating significance nr {}/{}".format(i, ncombinations))
            z = roostatsSignificance(s, b, db, flatsys=bkgsys)
            if z > max:
                max = z
                maxi = i
        return(maxi, max)

    def writeCuts(self, **kwargs):
        outdir = kwargs.get("outdir", "output")
        with open("{}/allcuts.txt".format(outdir), "w") as of:
            for (__, i, __, __, __) in getYields(self):
                of.write("{};{}\n".format(i, self.getCutExprFromIndex(i)))

    def splitGen(self):
        """
        create instances for each tree
        return True or False if splitting is applied
        """
        if not self.splitTrees:
            return False
        if self.subGens != []:
            return True
        for (filename, treename), customname in zip(self.trees, self.treenames):
            treegen = copy.deepcopy(self)
            treegen.splitTrees = False
            if not customname:
                treegen.name = self.name+"_"+treename
            else:
                treegen.name = self.name+"_"+customname
            if treegen.name in [i.name for i in self.subGens]:
                raise Exception("Name {} does already exist - "
                                "please specify names for your "
                                "trees with addTree(..., name=\"myname\") "
                                "when using splitTrees=True in case "
                                "your trees have identical names".format(treegen.name))
            treegen.txtfile = treegen.name+".txt"
            treegen.trees = [(filename, treename)]
            treegen.vars = treegen.getVars()
            self.subGens.append(treegen)
        return True

    def splitBins(self):
        """
        create instances for each bin
        return true or false if splitting is applied
        """
        if self.splitGen(): # first splitGen
            return False
        if not self.bins:
            return False
        if self.binGens != []:
            return True
        for i, bin in enumerate(self.bins):
            bingen = copy.deepcopy(self)
            bingen.splitTrees = False
            bingen.bins = []
            bingen.name = self.name+"_bin{}".format(i)
            bingen.txtfile = bingen.name+".txt"
            bingen.vars = bingen.getVars()
            bingen.weight = "({})*({})".format(bingen.weight, bin.expr)
            self.binGens.append(bingen)
        return True

    def openFiles(self):
        if self.splitGen():
            for subGen in self.subGens:
                subGen.openFiles()
            return
        if self.splitBins():
            for binGen in self.binGens:
                binGen.openFiles()
            return
        self.f = open(self.workdir+"/_txt/"+self.txtfile)

    def closeFiles(self):
        if self.splitGen():
            for subGen in self.subGens:
                subGen.closeFiles()
                return
        if self.splitBins():
            for binGen in self.binGens:
                binGen.closeFiles()
                return
        if self.f:
            self.f.close()

    def nextLine(self, info, checkRequirements):
        n, ry, y, dy = (None, 0, 0, 0)
        if self.splitGen():
            for subGen in self.subGens:
                line = subGen.nextLine(info, checkRequirements)
                if line is None:
                    return None
                info, _n, _ry, _y, _dy = line
                if (not n is None) and (not n == _n):
                    raise Exception("Lines not matching")
                n, ry, y, dy = (_n, ry+_ry, y+_y, math.sqrt(dy*dy+_dy*_dy))
                checkRequirements(subGen, _ry, _y, _dy, info)
        elif self.splitBins():
            for binGen in self.binGens:
                line = binGen.nextLine(info, checkRequirements)
                if line is None:
                    return None
                info, _n, _ry, _y, _dy = line
                if (not n is None) and (not n == _n):
                    raise Exception("Lines not matching")
                n, ry, y, dy = (_n, ry+_ry, y+_y, math.sqrt(dy*dy+_dy*_dy))
        else:
            line = self.f.readline()
            if not line:
                return None
            return tuple([info]+[float(i) for i in line.strip().split()])
        return info, n, ry, y, dy

    def listTargets(self):
        targets = ""
        if self.splitTrees:
            self.splitGen()
            for subGen in self.subGens:
                targets += subGen.listTargets()
        else:
            if self.bins:
                self.splitBins()
                for bingen in self.binGens:
                    targets += bingen.listTargets()
            else:
                targets += self.workdir+"/_txt/"+self.txtfile+" "
        return targets

    def getNCombinations(self):
        ncombinations = 1
        for cut in self.cuts:
            ncombinations *= cut.ncuts
        return ncombinations

    def dryRun(self):
        """show what would be done"""
        ncombinations = self.getNCombinations()
        print("will scan {} cut combinations".format(ncombinations))
        print("this will need approximately {:.1f} MiB of memory".format(3.0*float(ncombinations)*4.0/1024.0/1024.0))
        print("filling the roc curves will need something in the order of {:.0f} minutes".format(0.00007*float(ncombinations)/60))
        print("(largely depending on your storage/cpu)")

    def generate(self, **kwargs):
        """
        generate the c code
        """
        if self.trees == []:
            raise ValueError("No trees given, won't generate code!")
        if self.splitTrees:
            self.splitGen()
            for subGen in self.subGens:
                subGen.generate()
            return
        if self.bins:
            self.splitBins()
            for binGen in self.binGens:
                binGen.generate()
            return
        self.vars = self.getVars()
        self.generateLoop()

    def generateLoop(self):
        """
        generate c code which is using loop over all events and scan all
        cut combinations for each event
        """
        ncombinations = 1
        for cut in self.cuts:
            ncombinations *= cut.ncuts
        out="""#include <iostream>
#include <fstream>
#include <vector>
#include "math.h"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"

using namespace std;

int main() {
"""
        outl = []
        p = outl.append
        p("TChain* t = new TChain();")
        for filename, treename in self.trees:
            p("t->AddFile(\"{0}\", -1, \"{1}\");".format(filename, treename))
        for var,type in self.vars:
            if "vector<" in type:
                p("{}* {} = 0;".format(type, var))
            else:
                p("{} {};".format(type, var))
        for cut in self.cuts:
            p("int i{0}, i{0}start, i{0}end;".format(cut.varname))
        for var,type in self.vars:
            p("t->SetBranchAddress(\"{0}\", &{0});".format(var))
        for cut in self.cuts:
            first = 1
            liststring = ""
            for v in cut.cutvalues:
                if first:
                    liststring+="{}".format(v)
                    first = 0
                else:
                    liststring+=", {}".format(v)
            if cut.type == "expr":
                type = "Double_t"
            elif "vector<" in cut.type:
                type = cut.type.split("<")[1].replace(">","")
            else:
                type = cut.type
            p("std::vector<{}> {}cuts {{{}}};".format(type, cut.varname, liststring))
        for cut in self.cuts:
            p("std::vector<bool> {}pass({},0);".format(cut.varname, cut.ncuts))
        p("std::vector<float> yields({}, 0);".format(ncombinations))
        p("std::vector<float> rawyields({}, 0);".format(ncombinations))
        p("std::vector<float> sumw2s({}, 0);".format(ncombinations))
        p("int combinationIndex;")
        p("""for (int i=0; i<t->GetEntries(); i++) {
    t->GetEntry(i);""")
        p("  if (!(i%{})) std::cout << \"Processing event \" << i << \"/\" << t->GetEntries() << std::endl;".format(self.statusInterval))
        # indent by 2 spaces
        for l in outl:
            out+="{:<2}{}\n".format("",l)
        outl = []
        p = outl.append
        p("if (!({})) continue;".format(self.weight))
        p("if (!({})) continue;".format(self.presel))
        for cut in self.cuts:
            p("i{}start = 0;".format(cut.varname))
            p("i{0}end = {1};".format(cut.varname, cut.ncuts))
        for cut in self.cuts:
            p("for (i{0}=0; i{0}<{1}; i{0}++) {{".format(cut.varname, cut.ncuts))
            # vector corrections:
            vector_check_size = ""
            if "vector<" in cut.type:
                vector_name, vector_i = cut.varname.split("_")
                vector_check_size = "{0}->size()>{1} && ".format(vector_name, vector_i)
            if cut.type == "expr":
                for var,type in cut.vars:
                    if "vector<" in type:
                        vector_name = var.split("[")[0]
                        vector_i = var.split("[")[1].replace("]","")
                        vector_check_size += "{0}->size()>{1} && ".format(vector_name, vector_i)
            my_expr = cut.expr.replace("[","->at(").replace("]",")") # corrected expr string (for vectors)
            # end corrections
            p("  if ({0}{1} {2} {3}cuts[i{3}]) {{".format(vector_check_size, my_expr, cut.comparator, cut.varname))
            p("    {0}pass[i{0}] = 1;".format(cut.varname))
            p("  }")
            p("  else {")
            p("    {0}pass[i{0}] = 0;".format(cut.varname))
            # -> leave this out for safety since it doesn't seem to improve performance much
            # if not cut.low:
            # # no need to check smaller values later (cut value was upper bound and not passed)
            #     p("    i{0}start = i{0};".format(cut.varname))
            # if cut.low:
            # # no need to check higher values later (cut value was lower bound and not passed)
            #     p("    i{0}end = i{0};".format(cut.varname))
            p("  }")
            p("}")
        for i,cut in enumerate(self.cuts):
            p("{1:<{2}}for (i{0}=i{0}start; i{0}<i{0}end; i{0}++) {{".format(cut.varname, "", i*2))
            p("{0:<{1}}if (!{2}pass[i{2}]) continue;".format("", (i+1)*2, cut.varname))
        funexpr = self.make1dIndexExpression([i.varname for i in self.cuts],
                                             [i.ncuts for i in self.cuts])
        p("{0:<{1}}combinationIndex = {2};".format("", len(self.cuts)*2, funexpr))
        p("{0:<{1}}rawyields[combinationIndex] ++;".format("", len(self.cuts)*2))
        p("{0:<{1}}yields[combinationIndex] += {2};".format("", len(self.cuts)*2, self.weight))
        p("{0:<{1}}sumw2s[combinationIndex] += {2}*{2};".format("", len(self.cuts)*2, self.weight))

        # --------------------------------------------------debug
        if self.doDebug:
            firstcut = self.cuts[0]
            cutexpr = "{0}{1}%f".format(firstcut.expr, firstcut.comparator, firstcut.cutvalues)
            for cut in self.cuts[1:]:
                cutexpr += "&&{0}{1}{2}".format(cut.expr, cut.comparator, "%f" if cut.type == "Float_t" else "%d")
            ccutvaluelist = "{0}cuts[i{0}]".format(firstcut.varname)
            for cut in self.cuts[1:]:
                ccutvaluelist += ", {0}cuts[i{0}]".format(cut.varname)
            p("std::cout << combinationIndex << \" \" << TString::Format(\"{2}\",{3}).Data() << std::endl;"
              .format("",len(self.cuts)*2, cutexpr, ccutvaluelist))
        # --------------------------------------------------

        #p("std::cout << weight << std::endl;")
        # close brackets
        for i,cut in reversed([ j for j in enumerate(self.cuts)]):
            p("{0:<{1}}}}".format("",2*i))
        # indent by 4 spaces
        for l in outl:
            out+="{:<4}{}\n".format("",l)
        out+="  }\n"
        outl = []
        p = outl.append
        # write yields and errors to file
        p("std::ofstream of;")
        p("of.open(\"{}/_txt/{}\", ios::out | ios::trunc);".format(self.workdir, self.txtfile))
        p("for (int i=0; i<yields.size(); i++) {")
        p("  of << i << \" \" << rawyields[i] << \" \"<< yields[i] << \" \" << sqrt(sumw2s[i]) << std::endl;")
        p("}")
        p("of.close();")
        # indent by 2 spaces
        for l in outl:
            out+="{:<2}{}\n".format("",l)
        out+="}\n"

        with open(self.workdir+"/_c/"+self.name+".c", "w") as f:
            f.write(out)

# --------------------------------------------------
# general functions using the classes
# --------------------------------------------------

def roostatsSignificance(s, b, dbstat, **kwargs):
    flatsys = kwargs.get("flatsys", 0.25)
    try:
        return ROOT.RooStats.NumberCountingUtils.BinomialExpZ(s, b, math.sqrt(dbstat**2+(flatsys*b)**2)/b)
    except ZeroDivisionError:
        #print("Division by Zero! Setting z=0")
        return 0

def fillRoc(roc, cuts, effS, effB, cutN, **kwargs):
    """
    roc is list of tuples (effS, effB)
    cuts is list of cut combination numbers
    effS is lower edge of the bin
    """
    zs = kwargs.get("zs", None)
    z = kwargs.get("z", None)
    prevEffS = None
    prevEffB = None
    for i, (effS0, effB0) in enumerate(roc):
        if prevEffS is None:
            pass # first bin
        elif effS < prevEffS or effS > effS0:
            pass # wrong bin
        # check if bkg suppression is better and fill previous bin
        elif effB < prevEffB:
            roc[i-1] = (prevEffS, effB)
            cuts[i-1] = cutN
            if z and zs:
                zs[i-1] = z
            break
        prevEffS = effS0
        prevEffB = effB0
    return (i-1)

def makeRocGraph(roc):
    g = ROOT.TGraph()
    for i, (effS, effB) in enumerate(roc):
        g.SetPoint(i, effS, 1-effB)
    return g

def makeZGraph(roc, cuts, sig, bkg, **kwargs):
    vsEffB = kwargs.get("vsEffB", False)
    absoluteB = kwargs.get("absoluteB", False)
    b0 = kwargs.get("b0", None)
    zs = kwargs.get("zs", None)
    g = ROOT.TGraph()
    for i, ((effS, effB), cutn) in enumerate(zip(roc, cuts)):
        if cutn == None:
            continue
        if not zs: # much slower since it has to open up all txt files again
            __, _b, _db = bkg.getYieldFromFiles(cutn)
            __, _s, __ = sig.getYieldFromFiles(cutn)
            if _b < 0: # don't use negative bkg yields
                continue
            z = roostatsSignificance(_s, _b, _db)
        else:
            z = zs[i]
        if z < 0: # don't use negative significances
            continue
        if not vsEffB: # z vs effS
            g.SetPoint(g.GetN(), effS, z)
        else: # z vs effB
            if not (effB > 0): # leave out, since it will make problems when plotting with logx
                continue
            if absoluteB:
                g.SetPoint(g.GetN(), effB*b0, z)
            else:
                g.SetPoint(g.GetN(), effB, z)
    return g

def resetInfo(info):
    "dummy definition - to be overwritten in config"
    pass

def checkRequirements(g, ry, y, dy, info):
    "dummy definition - to be overwritten in config"
    pass

def getYields(g):
    """
    generator function that yields all values linewise
    problem: only one instance can exist?
    -> TODO: open up the files here
    """
    info = {}
    g.openFiles()
    # TODO: make this nicer - e.g nextLine as an iterator
    while True:
        resetInfo(info)
        nextline = g.nextLine(info, checkRequirements)
        if nextline is None:
            break
        yield nextline
    g.closeFiles()

def getSignificance(n, sigGen, bkgGen, **kwargs):
    bkgsys = kwargs.get("bkgsys", 0.25)
    __, s, ds = sigGen.getYieldFromFiles(n)
    __, b, db = bkgGen.getYieldFromFiles(n)
    return roostatsSignificance(s, b, db, flatsys=bkgsys)

def getSignificanceUsingProject(cutCombination, sigGen, bkgGen, **kwargs):
    expr = kwargs.get("cutexpr", None)
    bkgsys = kwargs.get("bkgsys", 0.25)
    if expr:
        __, s, ds = sigGen.getYieldUsingProject(0, cutexpr=expr)
        __, b, db = bkgGen.getYieldUsingProject(0, cutexpr=expr)
    else:
        __, s, ds = sigGen.getYieldUsingProject(cutCombination)
        __, b, db = bkgGen.getYieldUsingProject(cutCombination)
    return roostatsSignificance(s, b, db, flatsys=bkgsys)

def writeSignificances(sigGen, bkgGen, **kwargs):
    limit = kwargs.get("limit", -1)
    threshold = kwargs.get("threshold", -1)
    outdir = kwargs.get("outdir", "output")
    ncombinations = sigGen.getNCombinations()
    with open("{}/significances.txt".format(outdir), "w") as of:
        for (__, _is, rs, s, ds), (info, i, rb, b, db) in itertools.izip(getYields(sigGen), getYields(bkgGen)):
            if not i%1000000:
                print("Calculating significance nr {}/{}".format(i, ncombinations))
            z = roostatsSignificance(s, b, db)
            if z > threshold:
                of.write("{} {} {} {}\n".format(i, s, b, z))

def doRocStudy(sigGen, bkgGen, **kwargs):
    outdir = kwargs.get("outdir", "output")
    requirements = kwargs.get("requirements", ["nom", "bulk"])
    reqfunctions = kwargs.get("reqfunctions", {})

    if not os.path.exists(outdir):
        os.makedirs(outdir)

    __,s0,__ = sigGen.getYieldUsingProject(0, cutexpr="1")
    __,b0,__ = bkgGen.getYieldUsingProject(0, cutexpr="1")

    # roc curve filled with effB = 1 for all effS bins
    rocs = {}
    zs = {}
    cuts = {}
    yields = {}
    for req in requirements:
        rocs[req] = [(0.01*i, 1.0) for i in range(101)]
        zs[req] = [0 for i in range(len(rocs[req]))]
        cuts[req] = [0 for i in range(len(rocs[req]))]
        yields[req] = [(0., 0., 0., 0.) for i in range(len(rocs[req]))]

    # loop over all cut combinations and calculate effS, effB, z
    # and fill roc curves
    bestz = 0
    bestzn = None
    ncombinations = sigGen.getNCombinations()
    for (__, _is, rs, s, ds), (info, i, rb, b, db) in itertools.izip(getYields(sigGen), getYields(bkgGen)):
        if not _is%10000:
            print("Testing cut combination {:.0f}/{:.0f}".format(_is, ncombinations))
        if not _is == i:
            raise Exception("Lines not matching!")
        effS = s/s0
        effB = b/b0

        if not b > 0: # require this always
            continue

        z = roostatsSignificance(s, b, db)

        # fill roc curve for each requirement
        for req in requirements:
            try:
                reqfunctions[req]
            except KeyError:
                raise KeyError("No requirement function for \"{}\" definied!".format(req))
            if reqfunctions[req](info, s, rs, ds, b, rb, db):
                effsbin = fillRoc(rocs[req], cuts[req], effS, effB, i, zs=zs[req], z=z)
                yields[req][effsbin] = (s, ds, b, db)

    # create TGraphs and write them to root file
    outf = ROOT.TFile.Open(outdir+"/graphs.root","RECREATE")
    rocGraphs = {}
    zEffsGraphs = {}
    zEffbGraphs = {}
    for req in requirements:
        rocGraphs[req] = makeRocGraph(rocs[req])
        zEffsGraphs[req] = makeZGraph(rocs[req], cuts[req],
                                      sigGen, bkgGen, zs=zs[req])
        zEffbGraphs[req] = makeZGraph(rocs[req], cuts[req],
                                      sigGen, bkgGen, zs=zs[req],
                                      vsEffB=True, absoluteB=True, b0=b0)

        rocGraphs[req].SetTitle("roc_{}".format(req))
        zEffsGraphs[req].SetTitle("zEffs_{}".format(req))
        zEffbGraphs[req].SetTitle("zEffb_{}".format(req))
        rocGraphs[req].Write("roc_{}".format(req))
        zEffsGraphs[req].Write("zEffs_{}".format(req))
        zEffbGraphs[req].Write("zEffb_{}".format(req))
    outf.Close()

    # write out effS, effB, z, cutn, cutstring seperated by semicolons
    for req in requirements:
        with open(outdir+"/"+req+".csv", "w") as of:
            of.write("#effS;effB;z;cutn;cutstring\n")
            for (effS, effB), cut, z in zip(rocs[req], cuts[req], zs[req]):
                of.write("{};{};{};{:.0f};{}\n".format(effS, effB, z, cut, sigGen.getCutExprFromIndex(cut)))
        with open(outdir+"/"+req+"_yields.csv", "w") as of:
            of.write("#cutn;s;ds;b;db\n")
            for (s, ds, b, db), cut in zip(yields[req], cuts[req]):
                of.write("{:.0f};{};{};{};{}\n".format(cut, s, ds, b, db))

def redoRocNewLumi(sigGen, bkgGen, inputlumi, outputlumi, **kwargs):
    outdir = kwargs.get("outdir", "output")
    requirements = kwargs.get("requirements", ["nom", "bulk"])
    #reqfunctions = kwargs.get("reqfunctions", {})
    for req in requirements:
        print("requirement {}".format(req))
        with open(outdir+"/"+req+".csv") as f:
            of = open("{}/{}_{:.0f}.csv".format(outdir, req, outputlumi), "w")
            yf = open("{}/{}_yields.csv".format(outdir, req))
            for l, ly in itertools.izip(f, yf):
                if "#" in l:
                    of.write(l)
                    continue
                effS, effB, z, cut, expr = l.strip().split(";")
                ycut, s, ds, b, db = [float(i) for i in ly.strip().split(";")]
                cut = int(float(cut))
                ycut = int(cut)
                if not cut == ycut:
                    raise Exception("Lines not matching: {} != {}".format(cut, ycut))
                s, ds, b, db = [x/float(inputlumi)*float(outputlumi) for x in [s, ds, b, db]]
                z = roostatsSignificance(s, b, db)
                of.write("{};{};{};{};{}\n".format(effS, effB, z, cut, expr))
            of.close()
            yf.close()

def niceColors():
    cl = [ROOT.kBlack, ROOT.kRed, ROOT.kOrange-3, ROOT.kGreen+3, ROOT.kBlue, ROOT.kMagenta, ROOT.kSpring+9, ROOT.kBlue+6]
    for c in cl:
        yield c

def plotGraphs(inputfile, outdir, **kwargs):
    requirements = kwargs.get("requirements", ["nom", "bulk"])

    f = ROOT.TFile.Open(inputfile)

    rocGraphs = {}
    zEffbGraphs = {}
    zEffsGraphs = {}
    for req in requirements:
        rocGraphs[req] = f.Get("roc_{}".format(req))
        zEffbGraphs[req] = f.Get("zEffb_{}".format(req))
        zEffsGraphs[req] = f.Get("zEffs_{}".format(req))

    # plot TGraphs
    c = ROOT.TCanvas()
    # roc curves
    first = 1
    for col, req in zip(niceColors(), requirements):
        g = rocGraphs[req]
        g.SetLineColor(col)
        if first:
            g.Draw("apl")
            g.GetHistogram().SetMinimum(0.99999)
            g.GetHistogram().SetMaximum(1.000001)
            g.GetXaxis().SetRangeUser(0,0.5)
            first = 0
            continue
        g.Draw("pl same")
    c.BuildLegend()
    c.SaveAs(outdir+"/rocs.pdf")
    c.Clear()
    # z vs effs
    first = 1
    for col, req in zip(niceColors(), requirements):
        g = zEffsGraphs[req]
        g.SetLineColor(col)
        if first:
            g.Draw("apl")
            first = 0
            continue
        g.Draw("pl same")
    c.BuildLegend()
    c.SaveAs(outdir+"/zvseffs.pdf")
    c.Clear()
    # z vs effb
    first = 1
    c.SetLogx()
    for col, req in zip(niceColors(), requirements):
        g = zEffbGraphs[req]
        g.SetLineColor(col)
        if first:
            g.Draw("apl")
            first = 0
            continue
        g.Draw("pl same")
    c.BuildLegend()
    c.SaveAs(outdir+"/zvseffb.pdf")
    c.Clear()
