#!/usr/bin/env python

import argparse
import os

from generator import generator

parser = argparse.ArgumentParser(description='Generate Makefile',formatter_class=argparse.ArgumentDefaultsHelpFormatter)

parser.add_argument('configfile')
parser.add_argument('--workdir', help='Working directory for Makefile and intermediate files', default='.')
parser.add_argument('-c', help='execute this command before executing the config file', default='')

args = parser.parse_args()

if not os.path.exists(args.workdir+"/_c"):
    os.mkdir(args.workdir+"/_c")

if not os.path.exists(args.workdir+"/_txt"):
    os.mkdir(args.workdir+"/_txt")

exec(args.c)
execfile(args.configfile)

config = {
    "configfile" : args.configfile,
    "targets" : sig.listTargets()+bkg.listTargets(),
    "workdir" : args.workdir,
    "command" : "-c \"{}\"".format(args.c) if args.c else ""
}

Makefile="""
ARGS := {configfile} {command} --workdir {workdir}

CXXFlAGS := -O2 -std=c++11 -Wl,--no-as-needed $(shell root-config --libs --ldflags --cflags)

TARGETS := {targets}

all: $(TARGETS)

{workdir}/_c/%.c: $(ARRGHPATH)/magic.py {configfile}
	$(ARRGHPATH)/magic.py -g $(ARGS)

{workdir}/_c/%: {workdir}/_c/%.c
	g++ $(CXXFlAGS) -o $@ $^

{workdir}/_txt/%.txt: {workdir}/_c/%
	$<

clean:
	rm -f {workdir}/_c/* {workdir}/_txt/*

.SECONDARY:
""".format(**config)

setupfile="""
export ARRGHARGS="{configfile} {command} --workdir {workdir}"
alias magic.py="magic.py $ARRGHARGS"
""".format(command=config["command"].replace("\"","\\\""),
           **({k:v for k,v in config.items() if not k == "command"}))

#""".format(command="-c \\\"{}\\\"".format(args.c) if args.c else "", **({k:v for k,v in config.items() if not k == "command"}))

with open("Makefile", "w") as f:
    f.write(Makefile)

with open("thisarrgh.sh", "w") as f:
    f.write(setupfile)
