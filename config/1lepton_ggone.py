"""
Objects to define here:

sig, bkg:                     generator instances that define the trees and cuts to be used for signal and background

requirements:                 dictionary of requirements to check before filling the ROC curves
resetInfo, checkRequirements: functions that fill the information needed to check the requirements
reqfunctions:                 dictionary of functions that check for each requirement
"""

if not 'signalpoint' in dir():
    signalpoint = "onestepGG_1385_705_25"
if not 'njet' in dir():
    njet = 5
if not 'lumi' in dir():
    lumi = 3000

# splitTrees means that the code is ran for each Tree (which is added with addTree) seperately
# This can be used later to check requirements on the single components of the background
bkg = generator("background", splitTrees=True, statusInterval=10000)

sig = generator("signal", statusInterval=100)

treedir = "/project/etp3/nhartmann/trees/03-02-newVariables-v0/allTrees.root"

bkg.addTree(treedir, "diboson_NoSys")
bkg.addTree(treedir, "singletop_NoSys")
bkg.addTree(treedir, "ttbar_NoSys")
bkg.addTree(treedir, "wjets_NoSys")
bkg.addTree(treedir, "zjets_NoSys")

sig.addTree(treedir, signalpoint+"_NoSys")

def configCuts(gen):
    gen.addCuts("lep1Pt", "Float_t", [35.])
    gen.addCuts("jet1Pt", "Float_t", [30+50*j for j in range(5)])

    if njet == 3:
        gen.addCuts("jet3Pt", "Float_t", [30+50*j for j in range(5)])
        gen.addCuts("nJet30", "Int_t", [2])
    if njet == 4:
        gen.addCuts("jet4Pt", "Float_t", [30+50*j for j in range(5)])
        gen.addCuts("nJet30", "Int_t", [3])
    if njet == 5:
        gen.addCuts("jet5Pt", "Float_t", [30+50*j for j in range(5)])
        gen.addCuts("nJet30", "Int_t", [4])
    if njet == 6:
        gen.addCuts("jet6Pt", "Float_t", [30+50*j for j in range(5)])
        gen.addCuts("nJet30", "Int_t", [5])

    gen.addCuts("JetAplanarity", "Float_t", [j*0.01 for j in range(10)])
    #gen.addCuts("met", "Float_t", [200.0+50*j for j in range(7)])
    gen.addCuts("met", "Float_t", [200])
    gen.addCuts("mt", "Float_t", [75.0+j*50 for j in range(8)])
    gen.addCuts("met/meffInc30",
              "expr", [0.,0.1,0.2,0.3,0.4],
              vars=[("met", "Float_t"), ("meffInc30", "Float_t")],
              varname="metovermeff")
    #gen.addCuts("meffInc30", "Float_t", [200*j for j in range(20)])

    # it is possible to scan multiple bins at once for each cut combination
    # example1: multiple bins in 1 var:
    # gen.addCuts("meffInc30", "Float_t", [0, 200, 1000, 2000], binned=True, overflow=True)
    # example2: custom bins:
    # gen.addVar("meffInc30", "Float_t")
    # gen.addBin("meffInc30>=200&&meffInc30<1000")
    # gen.addBin("meffInc30>=1000&&meffInc30<2000")
    # gen.addBin("meffInc30>2000")

    gen.setWeightExpression("{}*genWeight*eventWeight*leptonWeight*triggerWeight".format(lumi),
                            [("genWeight", "Double_t"),
                             ("eventWeight", "Double_t"),
                             ("leptonWeight", "Double_t"),
                             ("triggerWeight", "Double_t")])

    # optionally an overall preselection which is not taken into account for calculating the efficiencies can be given
    # this allows better comparison for different setups with different preselections
    # example:
    # gen.setPreselectionExpression("nBJet30_MV1==0", [("nBJet30_MV1", "Int_t")])

configCuts(sig)
configCuts(bkg)

# when creating roc curves, check these requirements (defined below) and produce one roc curve for each
requirements = ["nom", "bulk", "wt0", "alg0", "30p", "2bg", "5bg"]

# set the infos for the requirements
def resetInfo(info):
    """
    this is called bevore all requirements are checked
    and before the loop over all sub generators (background trees/processes) is performed
    """
    info["no_ttbar"] = False
    info["no_wjets"] = False
    info["no_singletop"] = False
    info["no_diboson"] = False
    info["bulk_ttbar"] = False
    info["bulk_wjets"] = False

def checkRequirements(g, ry, y, dy, info):
    """
    this is called for each region candidate and for each (!) sub generator/background tree
    """
    if ("ttbar" in g.name) and (not (y>0)):
        info["no_ttbar"] = True
    if ("wjets" in g.name) and (not (y>0)):
        info["no_wjets"] = True
    if ("singletop" in g.name) and (not (y>0)):
        info["no_singletop"] = True
    if ("diboson" in g.name) and (not (y>0)):
        info["no_diboson"] = True
    if ("ttbar" in g.name) and ry>=10:
        info["bulk_ttbar"] = True
    if ("wjets" in g.name) and ry>=10:
        info["bulk_wjets"] = True

# define the requirements
reqfunctions = {}

# background > 0
reqfunctions["nom"] = lambda info, s, rs, ds, b, rb, db: b>0

# at least 10 raw ttbar events and 10 raw wjets events
reqfunctions["bulk"] = lambda info, s, rs, ds, b, rb, db: (info["bulk_ttbar"] and info["bulk_wjets"])

# w+jets > 0 and ttbar > 0
reqfunctions["wt0"] =lambda info, s, rs, ds, b, rb, db: not (info["no_ttbar"] or info["no_wjets"])

# w+jets > 0, ttbar > 0, singletop > 0, diboson > 0
reqfunctions["alg0"] = lambda info, s, rs, ds, b, rb, db: not (info["no_ttbar"]
                                                               or info["no_wjets"]
                                                               or info["no_singletop"]
                                                               or info["no_diboson"])

# mc stat uncertainty on total background < 30%
reqfunctions["30p"] =lambda info, s, rs, ds, b, rb, db: (db/b < 0.3)

# at least 5 weighted bkg events
reqfunctions["5bg"] =lambda info, s, rs, ds, b, rb, db: (b>=5)

# at least 2 weighted bkg events
reqfunctions["2bg"] =lambda info, s, rs, ds, b, rb, db: (b>=2)
