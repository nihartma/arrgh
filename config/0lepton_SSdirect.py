"""
Objects to define here:

sig, bkg:                     generator instances that define the trees and cuts to be used for signal and background

requirements:                 dictionary of requirements to check before filling the ROC curves
resetInfo, checkRequirements: functions that fill the information needed to check the requirements
reqfunctions:                 dictionary of functions that check for each requirement
"""

if not 'signalpoint' in dir():
    signalpoint = "Squark_700_400"
if not 'njet' in dir():
    njet = 3
if not 'lumi' in dir():
    lumi = 30000
print "Global scan parameters:"
print "  signal point =",signalpoint
print "  number of jets =",njet
print "  lumi =",lumi

# splitTrees means that the code is ran for each Tree (which is added with addTree) seperately
# This can be used later to check requirements on the single components of the background
bkg = generator("background", splitTrees=True, statusInterval=10000)

sig = generator("signal", statusInterval=100)

treename = "/etapfs01/atlashpc/jakobik/Ana_Results/Results_160916_Bg_p2666_NoSyst/MergedTrees/SusyFitterTree_NoSyst.root"
#treename = "/etapfs01/atlashpc/msimon03/test.root"

bkg.addTree(treename, "Other_NoSyst")
bkg.addTree(treename, "ttbar_NoSyst")
bkg.addTree(treename, "Znunu_NoSyst")
bkg.addTree(treename, "Wtaunu_NoSyst")

sig.addTree(treename, signalpoint+"_NoSyst")


def configCuts(gen):
    
    #Presel
    #met > 200
    #NEle == 0
    #NMuon == 0
    #NJet > 1
    #JetPt[0] > 250
    #DeltaPhiEtmissJetsMin > 0.4
    
    #gen.addCuts("NEle", "Int_t", [0], comparator="==")
    #gen.addCuts("NMuon", "Int_t", [0], comparator="==")

    gen.addCuts("JetPt[0]", "std::vector<double>", [250+50*j for j in range(4)]) # 250,300,350,400
    gen.addCuts("JetPt[1]", "std::vector<double>", [50+50*j for j in range(6)]) # 50,100,150,200,250,300
    gen.addCuts("JetPt[0]-JetPt[1]",
                "expr",
                [20*j for j in range(6)],
                vars=[("JetPt[0]", "std::vector<double>"), ("JetPt[1]", "std::vector<double>")],
                varname="Jet12dPt",
                comparator="<=") # 0,20,40,60,80,100
    
    if njet == 2:
        gen.addCuts("NJet", "UInt_t", [2], comparator="==")
        
    if njet == 3:
        gen.addCuts("NJet", "UInt_t", [3], comparator="==")
        gen.addCuts("JetPt[2]", "std::vector<double>", [50+50*j for j in range(5)]) # 50,100,150,200,250
        
    if njet == 4:
        gen.addCuts("NJet", "UInt_t", [4], comparator=">=")
        gen.addCuts("JetPt[2]", "std::vector<double>", [50+50*j for j in range(5)]) # 50,100,150,200,250
        gen.addCuts("JetPt[3]", "std::vector<double>", [50+50*j for j in range(4)]) # 50,100,150,200
        
    gen.addCuts("met", "Double_t", [250.0+50*j for j in range(6)]) # 250,300,350,400,450,500
    gen.addCuts("MEff", "Double_t", [200+200*j for j in range(10)]) # 200,400,...,2000
    gen.addCuts("Mt2[4]", "std::vector<double>", [50.0+j*50 for j in range(5)]) # 50,100,150,200,250,300,350
    
    # it is possible to scan multiple bins at once for each cut combination
    # example1: multiple bins in 1 var:
    # gen.addCuts("meffInc30", "Float_t", [0, 200, 1000, 2000], binned=True, overflow=True)
    # example2: custom bins:
    # gen.addVar("meffInc30", "Float_t")
    # gen.addBin("meffInc30>=200&&meffInc30<1000")
    # gen.addBin("meffInc30>=1000&&meffInc30<2000")
    # gen.addBin("meffInc30>2000")

    gen.setWeightExpression("{}*NoTagWeight*CrossSection*FilterEfficiency*genWeight".format(lumi),
                          [("NoTagWeight", "Double_t"),
                           ("CrossSection", "Double_t"),
                           ("FilterEfficiency", "Double_t"),
                           ("genWeight", "Double_t")])

    # optionally an overall preselection which is not taken into account for calculating the efficiencies can be given
    # this allows better comparison for different setups with different preselections
    # example:
    # gen.setPreselectionExpression("nBJet30_MV1==0", [("nBJet30_MV1", "Int_t")])

configCuts(sig)
configCuts(bkg)

# when creating roc curves, check these requirements (defined below) and produce one roc curve for each
# requirements = ["nom", "bulk", "wt0", "alg0", "30p", "2bg", "5bg"]
requirements = ["nom"]

# set the infos for the requirements
def resetInfo(info):
    """
    this is called bevore all requirements are checked
    and before the loop over all sub generators (background trees/processes) is performed
    """
    info["no_ttbar"] = False
    info["no_wjets"] = False
    info["no_singletop"] = False
    info["no_diboson"] = False
    info["bulk_ttbar"] = False
    info["bulk_wjets"] = False

def checkRequirements(g, ry, y, dy, info):
    """
    this is called for each region candidate and for each (!) sub generator/background tree
    """
    if ("ttbar" in g.name) and (not (y>0)):
        info["no_ttbar"] = True
    if ("wjets" in g.name) and (not (y>0)):
        info["no_wjets"] = True
    if ("singletop" in g.name) and (not (y>0)):
        info["no_singletop"] = True
    if ("diboson" in g.name) and (not (y>0)):
        info["no_diboson"] = True
    if ("ttbar" in g.name) and ry>=10:
        info["bulk_ttbar"] = True
    if ("wjets" in g.name) and ry>=10:
        info["bulk_wjets"] = True

# define the requirements
reqfunctions = {}

# background > 0
reqfunctions["nom"] = lambda info, s, rs, ds, b, rb, db: b>0

# at least 10 raw ttbar events and 10 raw wjets events
reqfunctions["bulk"] = lambda info, s, rs, ds, b, rb, db: (info["bulk_ttbar"] and info["bulk_wjets"])

# w+jets > 0 and ttbar > 0
reqfunctions["wt0"] =lambda info, s, rs, ds, b, rb, db: not (info["no_ttbar"] or info["no_wjets"])

# w+jets > 0, ttbar > 0, singletop > 0, diboson > 0
reqfunctions["alg0"] = lambda info, s, rs, ds, b, rb, db: not (info["no_ttbar"]
                                                               or info["no_wjets"]
                                                               or info["no_singletop"]
                                                               or info["no_diboson"])

# mc stat uncertainty on total background < 30%
reqfunctions["30p"] =lambda info, s, rs, ds, b, rb, db: (db/b < 0.3)

# at least 5 weighted bkg events
reqfunctions["5bg"] =lambda info, s, rs, ds, b, rb, db: (b>=5)

# at least 2 weighted bkg events
reqfunctions["2bg"] =lambda info, s, rs, ds, b, rb, db: (b>=2)
