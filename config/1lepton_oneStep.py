"""
Objects to define here:

sig, bkg:                     generator instances that define the trees and cuts to be used for signal and background

requirements:                 dictionary of requirements to check before filling the ROC curves
resetInfo, checkRequirements: functions that fill the information needed to check the requirements
reqfunctions:                 dictionary of functions that check for each requirement
"""

if not 'signalpoint' in dir():
    signalpoint = "GG_oneStep_1385_705_25"
if not 'njet' in dir():
    njet = 5
if not 'lumi' in dir():
    lumi = 30000
print "Global scan parameters:"
print "  signal point =",signalpoint
print "  number of jets =",njet
print "  lumi =",lumi

# splitTrees means that the code is ran for each Tree (which is added with addTree) seperately
# This can be used later to check requirements on the single components of the background
bkg = generator("background", splitTrees=True, statusInterval=10000)

sig = generator("signal", statusInterval=100)

treedir = "/home/msimon03/workspace/arrgh/trees"
treename = "allBkgs_T_06_03.root"
sigtreename = "allSignal_T_06_03_complete.root"

bkg.addTree(treedir+"/"+treename, "diboson_NoSys")
bkg.addTree(treedir+"/"+treename, "singletop_NoSys")
bkg.addTree(treedir+"/"+treename, "ttbar_NoSys")
bkg.addTree(treedir+"/"+treename, "ttv_NoSys")
bkg.addTree(treedir+"/"+treename, "wjets_Sherpa22_NoSys")
bkg.addTree(treedir+"/"+treename, "zjets_Sherpa22_NoSys")

sig.addTree(treedir+"/"+sigtreename, signalpoint+"_NoSys")


def configCuts(gen):
    
    gen.addCuts("nLep_signal", "Int_t", [1], comparator="==")
    gen.addCuts("nLep_base", "Int_t", [1], comparator="==")
#     gen.addCuts("lep1Pt", "Float_t", [0,35,100,200,350])
    gen.addCuts("lep1Pt", "Float_t", [0,35,70,100,150])
    gen.addCuts("jet1Pt", "Float_t", [100+50*j for j in range(8)]) # 100,150,...,500

    if njet == 2:
        gen.addCuts("jet2Pt", "Float_t", [30,80,130,180])
        gen.addCuts("nJet30", "Int_t", [2], comparator=">=")
    if njet == 3:
        gen.addCuts("jet3Pt", "Float_t", [30,80,130])
        gen.addCuts("nJet30", "Int_t", [3], comparator=">=")
    if njet == 4:
        gen.addCuts("jet4Pt", "Float_t", [30,50,70])
        gen.addCuts("nJet30", "Int_t", [4], comparator=">=")
    if njet == 5:
        gen.addCuts("jet5Pt", "Float_t", [30,50,70])
        gen.addCuts("nJet30", "Int_t", [5], comparator=">=")
    if njet == 6:
        gen.addCuts("jet6Pt", "Float_t", [30,50,70])
        gen.addCuts("nJet30", "Int_t", [6], comparator=">=")
    if njet == 7:
#         gen.addCuts("jet7Pt", "Float_t", [30,50])
        gen.addCuts("nJet30", "Int_t", [7], comparator=">=")
    if njet == 8:
#         gen.addCuts("jet8Pt", "Float_t", [30,50])
        gen.addCuts("nJet30", "Int_t", [8], comparator=">=")

#     gen.addCuts("JetAplanarity", "Float_t", [j*0.01 for j in range(10)]) # 0,0.01,0.02,...,0.09
    gen.addCuts("LepAplanarity", "Float_t", [j*0.01 for j in range(10)]) # 0,0.01,0.02,...,0.09
    gen.addCuts("met", "Float_t", [350+50*j for j in range(8)]) # 350,400,450,500,550,600,650,700
    gen.addCuts("mt", "Float_t", [100+j*50 for j in range(8)]) # 100,150,200,250,300,350,400,450
    gen.addCuts("met/meffInc30",
              "expr", [0.,0.1,0.2,0.3,0.4],
              vars=[("met", "Float_t"), ("meffInc30", "Float_t")],
              varname="metovermeff")
#     gen.addCuts("meffInc30", "Float_t", [200*j for j in range(30)])
#     gen.addCuts("meffInc30", "Float_t", [200+200*j for j in range(8)]) # 200,400,...,1600
    gen.addCuts("meffInc30", "Float_t", [0,1400,1600,1800,2000,2200,2400,2600])
    # new ttbar chi2 variables
#     gen.addCuts("minChi2", "Float_t", [0.,0.25,0.5,1.0,1.5,2.0,3.0], comparator="<")
#     gen.addCuts("M_t_had", "Float_t", [0,100,175,200,225,250,300,350])
#     gen.addCuts("M_t_lep", "Float_t", [0,100,175,200,225,250,300,350])
    # topness
#     gen.addCuts("topNess", "Float_t", [-4+2*j for j in range(10)]) # -4,-2,...,14
#     # number of bJets == 0
#     gen.addCuts("nBJet30_MV2c20", "Int_t", [0], comparator="==")
#     # number of bJets == 1
#     gen.addCuts("nBJet30_MV2c20", "Int_t", [1], comparator="==")
#     # number of bJets >= 2
#     gen.addCuts("nBJet30_MV2c20", "Int_t", [1]) # nBJet>1
    # b-veto on/off
#     gen.addCuts("nBJet30_MV2c10", "Int_t", [1,100], comparator="<") # nBJet<1 (==0 =veto) or <100 (no veto)
    gen.addCuts("nBJet30_MV2c10", "Int_t", [1], comparator="<") # nBJet<1 (==0 =veto)
    # DatasetNumber!=361327 (see "HACK" in generator)
#     gen.addCuts("DatasetNumber", "Int_t", [-1])
    # EventNumber!=3721565 (see "HACK" in generator)
#     gen.addCuts("EventNumber", "Int_t", [-1])
    
    # it is possible to scan multiple bins at once for each cut combination
    # example1: multiple bins in 1 var:
    # gen.addCuts("meffInc30", "Float_t", [0, 200, 1000, 2000], binned=True, overflow=True)
    # example2: custom bins:
    # gen.addVar("meffInc30", "Float_t")
    # gen.addBin("meffInc30>=200&&meffInc30<1000")
    # gen.addBin("meffInc30>=1000&&meffInc30<2000")
    # gen.addBin("meffInc30>2000")

#     gen.setWeightExpression("{}*genWeight*eventWeight*leptonWeight*triggerWeight".format(lumi),
#                             [("genWeight", "Double_t"),
#                              ("eventWeight", "Double_t"),
#                              ("leptonWeight", "Double_t"),
#                              ("triggerWeight", "Double_t")])

    gen.setWeightExpression("{}*genWeight*eventWeight*leptonWeight*bTagWeight*jvtWeight*SherpaVjetsNjetsWeight*pileupWeight*combinedTrigResult".format(lumi),
                          [("genWeight", "Double_t"),
                           ("eventWeight", "Double_t"),
                           ("leptonWeight", "Double_t"),
                           ("bTagWeight", "Double_t"),
                           ("jvtWeight", "Double_t"),
                           ("SherpaVjetsNjetsWeight", "Double_t"),
                           ("pileupWeight", "Double_t"),
                           ("combinedTrigResult", "Bool_t")])

    # optionally an overall preselection which is not taken into account for calculating the efficiencies can be given
    # this allows better comparison for different setups with different preselections
    # example:
    # gen.setPreselectionExpression("nBJet30_MV1==0", [("nBJet30_MV1", "Int_t")])

configCuts(sig)
configCuts(bkg)

# when creating roc curves, check these requirements (defined below) and produce one roc curve for each
# requirements = ["nom", "bulk", "wt0", "alg0", "30p", "2bg", "5bg"]
requirements = ["nom", "bulk", "30p", "alg0"]

# set the infos for the requirements
def resetInfo(info):
    """
    this is called bevore all requirements are checked
    and before the loop over all sub generators (background trees/processes) is performed
    """
    info["no_ttbar"] = False
    info["no_wjets"] = False
    info["no_singletop"] = False
    info["no_diboson"] = False
    info["bulk_ttbar"] = False
    info["bulk_wjets"] = False

def checkRequirements(g, ry, y, dy, info):
    """
    this is called for each region candidate and for each (!) sub generator/background tree
    """
    if ("ttbar" in g.name) and (not (y>0)):
        info["no_ttbar"] = True
    if ("wjets" in g.name) and (not (y>0)):
        info["no_wjets"] = True
    if ("singletop" in g.name) and (not (y>0)):
        info["no_singletop"] = True
    if ("diboson" in g.name) and (not (y>0)):
        info["no_diboson"] = True
    if ("ttbar" in g.name) and ry>=10:
        info["bulk_ttbar"] = True
    if ("wjets" in g.name) and ry>=10:
        info["bulk_wjets"] = True

# define the requirements
reqfunctions = {}

# background > 0
reqfunctions["nom"] = lambda info, s, rs, ds, b, rb, db: b>0

# at least 10 raw ttbar events and 10 raw wjets events
reqfunctions["bulk"] = lambda info, s, rs, ds, b, rb, db: (info["bulk_ttbar"] and info["bulk_wjets"])

# w+jets > 0 and ttbar > 0
reqfunctions["wt0"] =lambda info, s, rs, ds, b, rb, db: not (info["no_ttbar"] or info["no_wjets"])

# w+jets > 0, ttbar > 0, singletop > 0, diboson > 0
reqfunctions["alg0"] = lambda info, s, rs, ds, b, rb, db: not (info["no_ttbar"]
                                                               or info["no_wjets"]
                                                               or info["no_singletop"]
                                                               or info["no_diboson"])

# mc stat uncertainty on total background < 30%
reqfunctions["30p"] =lambda info, s, rs, ds, b, rb, db: (db/b < 0.3)

# at least 5 weighted bkg events
reqfunctions["5bg"] =lambda info, s, rs, ds, b, rb, db: (b>=5)

# at least 2 weighted bkg events
reqfunctions["2bg"] =lambda info, s, rs, ds, b, rb, db: (b>=2)
