#!/usr/bin/env python

import ROOT

import sys
import math
from generator import generator
import generator as gen
import argparse

parser = argparse.ArgumentParser(description='Generate code and extract information',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('configfile', default='config/1lepton_ggone.py')
parser.add_argument('-c', help='execute this command before executing the config file', default="")
parser.add_argument('-g', '--generate', action='store_true', help='generate code (signal and background)')
parser.add_argument('-t', '--targets', action='store_true', help='list txt file make targets')
parser.add_argument('-r', '--roc', action='store_true', help='do ROC curves')
parser.add_argument('-b', '--bkg', action='store_true', help='use background config (default)')
parser.add_argument('-s', '--sig', action='store_true', help='use signal config')
parser.add_argument('-v', help='var to plot/analyze')
parser.add_argument('-e', help='expression')
parser.add_argument('-n', metavar='n', help='use cut number n')
parser.add_argument('-y', action='store_true', help='print yields')
parser.add_argument('-z', action='store_true', help='print significance')
parser.add_argument('--project', action='store_true', help='use TTree::Project')
parser.add_argument('--threshold', help='Minimum significance threshold', default=-1)
parser.add_argument('--print-cuts', action='store_true', help='print cut string')
parser.add_argument('--max', action='store_true', help='find cut combination with best significance using RooStats::NumberCountingUtils::BinomialExpZ')
parser.add_argument('--write-z', action='store_true', help='get significances for all cut combinations and write them to a txt file')
parser.add_argument('--write-cuts', action='store_true', help='write cut expressions for all cut combinations to txt file')
parser.add_argument('--plot-graphs', action='store_true', help='plot roc graphs from rootfile')
parser.add_argument('--dry-run', action='store_true', help='show what will be done')
parser.add_argument('--redo-roc', action='store_true', help='recalculate z vs effs with new lumi given by --input-lumi and --output-lumi')
parser.add_argument('--input-lumi', type=float, help='old lumi')
parser.add_argument('--output-lumi', type=float, help='new lumi')


parser.add_argument('-d', '--dir', metavar='dir', help='output directory for roc curve files', default="output")
parser.add_argument('-w', '--workdir', metavar='workdir', help='working directory for intermediate files', default=".")

args = parser.parse_args()

# --------------------------------------------------
# configure trees and cuts
# --------------------------------------------------

print("Running the following command:")
print(args.c)

exec(args.c)

execfile(args.configfile)

if "checkRequirements" in dir():
    gen.checkRequirements = checkRequirements
if "resetInfo" in dir():
    gen.resetInfo = resetInfo

# --------------------------------------------------
# execute several things depending on args
# --------------------------------------------------

# set working directory
sig.setWorkdir(args.workdir)
bkg.setWorkdir(args.workdir)

# use signal or bkg config for further options?
if args.sig:
    g = sig
else:
    g = bkg

# list Makefile targets
if args.targets:
    print(sig.listTargets()+bkg.listTargets())

# generate c code
if args.generate:
    bkg.generate()
    sig.generate()

# print cutstring
if args.print_cuts:
    print(g.getCutExprFromIndex(args.n))

# print yields
if args.y:
    if args.project or args.e:
        if args.n:
            g.printYieldsProject(args.n)
        elif args.e:
            g.printYieldsProject(0, cutexpr=args.e)
        else:
            raise Exception("No cut string (-e) or cut number (-n) provided - can't print yields")
    else:
        if not args.n:
            raise Exception("No cut number given (-n)")
        #print(g.getYieldFromFiles(args.n))
        g.printYields(args.n)

# print significance
if args.z:
    if args.project or args.e:
        if args.n:
            print("z = {}".format(gen.getSignificanceUsingProject(args.n, sig, bkg)))
        elif args.e:
            print("z = {}".format(gen.getSignificanceUsingProject(0, sig, bkg, cutexpr=args.e)))
        else:
            raise Exception("No cut string (-e) or cut number (-n) provided - can't print significance")
    else:
        if not args.n:
            raise Exception("No cut number given (-n)")
        print("z = {}".format(gen.getSignificance(args.n, sig, bkg)))

# print cut combination with best significance
if args.max:
    index, z = g.getBestSignificance(sig, bkg, bkgsys=0.25)
    cutCombination = g.getCutExprFromIndex(index)
    print("Best Significance: {} for cut combination nr {} ({})".format(z, index, cutCombination))

if args.write_z:
    gen.writeSignificances(sig, bkg, outdir=args.dir, threshold=0.3)

if args.write_cuts:
    g.writeCuts()

if args.roc:
    gen.doRocStudy(sig, bkg, outdir=args.dir, requirements=requirements, reqfunctions=reqfunctions)

if args.plot_graphs:
    gen.plotGraphs(args.dir+"/graphs.root", args.dir, requirements=requirements, reqfunctions=reqfunctions)

if args.dry_run:
    g.dryRun()

if args.redo_roc:
    if not args.input_lumi:
        raise Exception("No input lumi given (--input-lumi)")
    if not args.output_lumi:
        raise Exception("No output lumi given (--output-lumi)")
    gen.redoRocNewLumi(sig, bkg, args.input_lumi, args.output_lumi, requirements=requirements)
